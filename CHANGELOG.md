# Changelog

#### 1.0.3

- Chore: support base `3.0`.

#### 1.0.2

- Chore: allow json ^3.

#### 1.0.1

- Chore: support `json` v2 and make the json dependency optional.

## 1.0.0

- Initial version, import modules from clean platform v0.3.38 and destill all
  big integer modules.
